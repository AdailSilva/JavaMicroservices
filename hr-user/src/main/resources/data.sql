INSERT INTO tb_user (name, email, password) VALUES ('Adail Silva', 'admin@adailsilva.com', '$2a$10$IkTNHxQlB.WQhUl1SNpE0OlmEcZ8zMCoQ2hr2tD7LZN6qISNfhUMW');
INSERT INTO tb_user (name, email, password) VALUES ('Ágatha Silva', 'agathasilva@adailsilva.com', '$2a$10$IkTNHxQlB.WQhUl1SNpE0OlmEcZ8zMCoQ2hr2tD7LZN6qISNfhUMW');
INSERT INTO tb_user (name, email, password) VALUES ('Davi Miguel', 'davimiguel@adailsilva.com', '$2a$10$IkTNHxQlB.WQhUl1SNpE0OlmEcZ8zMCoQ2hr2tD7LZN6qISNfhUMW');

INSERT INTO tb_role (role_name) VALUES ('ROLE_ADMINISTRATOR');
INSERT INTO tb_role (role_name) VALUES ('ROLE_OPERATOR');

INSERT INTO tb_user_role (user_id, role_id) VALUES (1, 1);
INSERT INTO tb_user_role (user_id, role_id) VALUES (1, 2);
INSERT INTO tb_user_role (user_id, role_id) VALUES (2, 2);

