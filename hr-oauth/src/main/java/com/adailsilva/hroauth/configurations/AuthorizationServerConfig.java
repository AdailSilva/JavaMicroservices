package com.adailsilva.hroauth.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@RefreshScope // For all classes that have access to some custom configuration
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Value("${oauth.client.id}")
	private String oAuthClientId;

	@Value("${oauth.client.secret}")
	private String oAuthClientSecret;
	
	@Value("#{'${oauth.scopes}'.split(',')}")
	private String[] oAuthScopes;

	@Value("${oauth.authorized.grant.types}")
	private String authorizedGrantTypes;
	
	@Value("${oauth.access.token.validity.seconds}")
	private int oAuthaccessTokenValiditySeconds;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenStore jwtTokenStore;

	@Autowired
	private JwtAccessTokenConverter accessTokenConverter;
	
	@Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
            .tokenKeyAccess("permitAll()")
            .checkTokenAccess("isAuthenticated()");
//            .allowFormAuthenticationForClients();
    }

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
			.withClient(oAuthClientId)
			.secret(bCryptPasswordEncoder.encode(oAuthClientSecret))
			.scopes(oAuthScopes)
			.authorizedGrantTypes(authorizedGrantTypes)
			.accessTokenValiditySeconds(oAuthaccessTokenValiditySeconds);
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.authenticationManager(authenticationManager)
			.tokenStore(jwtTokenStore)
			.accessTokenConverter(accessTokenConverter);
	}

}
