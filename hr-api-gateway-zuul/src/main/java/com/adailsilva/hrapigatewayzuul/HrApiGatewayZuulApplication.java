package com.adailsilva.hrapigatewayzuul;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@RefreshScope // For all classes that have access to some custom configuration
@EnableEurekaClient
@EnableZuulProxy
@SpringBootApplication
public class HrApiGatewayZuulApplication implements CommandLineRunner {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private Environment environment;

	@Value("${spring.application.name}")
	private String springApplicationName;

//	@Value("${server.port}")
//	private String serverPort;

	@Value("${eureka.client.service-url.defaultZone}")
	private String eurekaClientServiceUrlDefaultZone;

	@Value("${zuul.routes.oauth.service-id}")
	private String zuulRoutersOAuthServiceId;

	@Value("${zuul.routes.oauth.path}")
	private String zuulRoutersOAuthPath;

	@Value("#{'${zuul.routes.sensitive-headers}'.split(',')}")
	private List<String> zuulRoutesSensitiveHeaders;

	@Value("#{'${zuul.routes.oauth.sensitive-headers}'.split(',')}")
	private List<String> zuulRoutesOauthSensitiveHeaders;

	@Value("${zuul.routes.payroll.service-id}")
	private String zullRoutesPayrollServiceId;

	@Value("${zuul.routes.payroll.path}")
	private String zullRoutesPayrollPath;

	@Value("${zuul.routes.user.service-id}")
	private String zullRoutesUserServiceId;

	@Value("${zuul.routes.user.path}")
	private String zullRoutesUserPath;

	@Value("${zuul.routes.worker.service-id}")
	private String zullRoutesWorkerServiceId;

	@Value("${zuul.routes.worker.path}")
	private String zullRoutesWorkerPath;

	@Value("${hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds}")
	private String hystrixCommandDefaultExecutionIsolationThreadTimeoutInMilliseconds;

	@Value("${ribbon.ConnectTimeout}")
	private String ribbonConnectTimeout;

	@Value("${ribbon.ReadTimeout}")
	private String ribbonReadTimeout;

	@Value("${adailsilva.config}")
	private String remoteConfiguration;

	public static void main(String[] args) {
		SpringApplication.run(HrApiGatewayZuulApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Spring Application Name: " + springApplicationName);
		System.out.println("Server Port: " + environment.getProperty("local.server.port"));
		System.out.println("Eureka Client Service URL Default Zone: " + eurekaClientServiceUrlDefaultZone);
		System.out.println("Zuul Routes OAuth Service ID: " + zuulRoutersOAuthServiceId);
		System.out.println("Zull Routes Oauth Path: " + zuulRoutersOAuthPath);
		zuulRoutesSensitiveHeaders.stream().forEach(zuulRoutesSensitiveHeaders -> System.out
				.println("Zuul Routes Sensitive Headers: " + zuulRoutesSensitiveHeaders));
		zuulRoutesOauthSensitiveHeaders.stream().forEach(zuulRoutesOauthSensitiveHeaders -> System.out
				.println("Zuul Routes OAuth Sensitive Headers: " + zuulRoutesOauthSensitiveHeaders));
		System.out.println("Zull Routes Payroll Service ID: " + zullRoutesPayrollServiceId);
		System.out.println("Zull Routes Payroll Path: " + zullRoutesPayrollPath);
		System.out.println("Zull Routes User Service ID: " + zullRoutesUserServiceId);
		System.out.println("Zull Routes User Path: " + zullRoutesUserPath);
		System.out.println("Zull Routes Worker Service ID: " + zullRoutesWorkerServiceId);
		System.out.println("Zull Routes Worker Path: " + zullRoutesWorkerPath);
		System.out.println("Hystrix Command Default Execution Isolation Thread Timeout In Milliseconds: "
				+ hystrixCommandDefaultExecutionIsolationThreadTimeoutInMilliseconds);
		System.out.println("Ribbon Connect Timeout: " + ribbonConnectTimeout);
		System.out.println("Ribbon Read Timeout: " + ribbonReadTimeout);
		System.out.println("[ Remote Configuration ] --> " + remoteConfiguration);
		System.out.println("BCrypt: " + bCryptPasswordEncoder.encode("@Hacker101"));
	}

}
