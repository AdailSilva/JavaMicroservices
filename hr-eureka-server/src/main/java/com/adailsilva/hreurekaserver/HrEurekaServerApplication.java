package com.adailsilva.hreurekaserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@RefreshScope // For all classes that have access to some custom configuration
@EnableEurekaServer
@SpringBootApplication
public class HrEurekaServerApplication implements CommandLineRunner {
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private Environment environment;

	@Value("${spring.application.name}")
	private String springApplicationName;

	@Value("${server.port}")
	private String serverPort;

	@Value("${eureka.client.register-with-eureka}")
	private String eurekaClientRegisterWithEureka;

	@Value("${eureka.client.fetch-registry}")
	private String eurekaClientFetchRegistry;
	
	@Value("${adailsilva.config}")
	private String remoteConfiguration;

	public static void main(String[] args) {
		SpringApplication.run(HrEurekaServerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Spring Application Name: " + springApplicationName);
		System.out.println("Server Port: " + environment.getProperty("local.server.port"));
		System.out.println("Eureka Client Register With Eureka: " + eurekaClientRegisterWithEureka);
		System.out.println("Eureka Client Fetch Registry: " + eurekaClientFetchRegistry);
		System.out.println("[ Remote Configuration ] --> " + remoteConfiguration);
		System.out.println("BCrypt: " + bCryptPasswordEncoder.encode("@Hacker101"));
	}

}
